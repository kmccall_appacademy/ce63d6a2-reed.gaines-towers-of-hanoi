# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_accessor :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    counter = 0
    puts "welcome to towers of hanoi."
    puts self.render
    while counter < 1
      puts "where do you want to take a disk from?"
      $stdout.flush
      from_tower = gets.to_i
      puts "where do you want to put the disk?"
      $stdout.flush
      to_tower = gets.to_i
      move(from_tower, to_tower)
      puts self.render
      if won?
        counter += 1
        puts "congratulations"
      end
    end
  end

  def render
    top_row = @towers.map { |arr| arr.length >= 3 ? "~" : " "}
    mid_row = @towers.map { |arr| arr.length >= 2 ? "~" : " "}
    bot_row = @towers.map { |arr| arr.length >= 1 ? "~" : " "}
    return "#{top_row.join(" ")}\n#{mid_row.join(" ")}\n#{bot_row.join(" ")}\n_ _ _"
  end


  def move(from_tower, to_tower)
    if valid_move?(from_tower, to_tower)
      @towers[to_tower].push(@towers[from_tower].pop)
    else
      puts "invalid move!"
    end
  end

  def valid_move?(from_tower, to_tower)
    if from_tower > 2 || to_tower > 2
      return false
    elsif @towers[from_tower] == []
      return false
    elsif @towers[to_tower] == []
      return true
    elsif @towers[from_tower].last > @towers[to_tower].last
      return false
    end
    true
  end

  def won?
    if @towers[1] == [3, 2, 1] || @towers[2] == [3, 2, 1]
      return true
    end
    false
  end
end

if __FILE__ == $PROGRAM_NAME
  t = TowersOfHanoi.new
  t.play
end
